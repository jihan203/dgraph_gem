# frozen_string_literal: true

require 'grpc'
require_relative 'dgraph/version'
require_relative 'api_services_pb'

# D-graph ruby driver
module Dgraph
  def self.mutate(query_string)
    stub.mutate(Api::Mutation.new(set_json: query_string, commit_now: true))
  end

  def self.query(query_string, vars = {})
    stub.query(Api::Request.new(query: query_string, vars: vars))
  end

  # Altering database schema (needed for indexing, declaring reverse edges, etc)
  # example: Dgraph.alter("children: uid @reverse .")
  def self.alter(operation_string)
    stub.alter(Api::Operation.new(schema: operation_string))
  end

  def self.stub
    @stub ||= Api::Dgraph::Stub.new('0.0.0.0:9080', :this_channel_is_insecure)
  end
end
